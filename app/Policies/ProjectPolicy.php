<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Project;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given project can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Project  $project
     * @return bool
     */
    public function update(User $user, Project $project)
    {
        return $user->id == $project->user_id;
    }

    public function delete(User $user, Project $project)
    {
        return $user->id == $project->user_id;
    }

}
