<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['task'];

    public function getStatusAttribute($value) {
    	return ($value) ? 'done' : 'not done';
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
