<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Project;
use Auth;
use Gate;

class ProjectController extends Controller
{
    public function index() {

    	$project_list = Project::everything()->get();
    	return view('projects.index', compact('project_list'));
    }

    public function create() {
    	return view('projects.create');
    }

    public function store(Request $request) {
    	$user = Auth::user();
    	$project = new Project($request->all());
    	$user->addProject($project);
    	return redirect('projects');
    }

    public function show($id) {
		$project = Project::findOrFail($id);
    	return view('projects.show', compact('project'));    	
    }

    public function edit($id) {
    	$project = Project::findOrFail($id);
    	return view('projects.edit', compact('project'));
    }

    public function update(Request $request, $id) {

    	$project = Project::findOrFail($id);
		if (Gate::denies('update', $project)) {
            abort(403);
        }
    	$project->update($request->only('project_name', 'project_description'));

    	return redirect('projects');
    }

    public function destroy($id) {
    	$project = Project::findOrFail($id);
    	if (Gate::denies('delete', $project)) {
            abort(403);
        }
        $project->delete();
    	return redirect('projects');
    }

    public function createTask($id) {
    	return view('projects.create_task', compact('id'));
    }

    public function storeTask(Request $request, $id) {
    	$task = new \App\Task($request->only('task'));
    	$project = Project::findOrFail($id);
    	$project->addTask($task);
    	return redirect("projects/$id");
    }

}
