<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['web','auth']], function () {
	Route::resource('projects', 'ProjectController');
	Route::get('projects/{id}/tasks', 'ProjectController@taskList');
	Route::get('projects/{id}/create_task', 'ProjectController@createTask');
	Route::post('projects/{id}', 'ProjectController@storeTask');
});
Route::auth();

Route::get('/home', 'HomeController@index');
