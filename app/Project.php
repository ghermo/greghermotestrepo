<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
	protected $fillable = ['project_name', 'project_description'];
	protected $dates = ['deleted_at'];

	public function addTask($task) {
		$this->tasks()->save($task);
	}

	public function scopeEverything($query) {
		return $query->withTrashed();
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function tasks() {
		return $this->hasMany('App\Task');
	}

}
