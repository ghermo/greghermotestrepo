<?php

use App\Project;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjecTest extends TestCase
{

	use DatabaseTransactions;

	/** @test */
	function it_fetches_all_projects_including_trashed()
	{
		factory(Project::class, 3)->create();
		Project::first()->delete();
		$projects = Project::everything()->get();
		$this->assertCount(3, $projects);
	}

	/** @test */
	public function a_project_has_name() {
		$project = factory(Project::class)->create(['project_name' => 'Acme']);
		$this->assertEquals('Acme', $project->project_name);
	}

	/** @test */
	public function a_project_can_have_tasks() {
		$project = factory(Project::class)->create();
		$task1 = App\Task::create(['task'=>'Task 1']);
		$project->addTask($task1);
		$task2 = App\Task::create(['task'=>'Task 2']);
		$project->addTask($task2);

		$this->assertEquals(2, $project->tasks()->count());
	}

	/** @test */
	public function user_can_edit_his_project() {
		$project = factory(Project::class)->create(['project_name' => 'Acme']);
		$project->update(['project_name'=>'Acm']);
		$this->assertEquals('Acm', $project->project_name);
	}

	/** @test */
	public function user_can_remove_his_project() {
		$project = factory(Project::class)->create(['project_name' => 'Acme']);
		$project->delete();
		$this->assertEquals(1, $project->count());
		// $this->assertEquals();
	}

	/** @test */
	public function user_cannot_edit_others_project() {}

	/** @test */
	public function user_cannot_remove_others_project() {}	

	// it_can_add_task
}