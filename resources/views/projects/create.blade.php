@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Project</div>

                <div class="panel-body">
                    <form method="post" action="{{ url('projects') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
					<label for="project_name">Project Name:</label>
					<input type="text" name="project_name" class="form-control" />
					</div>

					<div class="form-group">
					<label for="project_description">Project Name:</label>
					<input type="text" name="project_description" class="form-control" />
					</div>

					<a href="{{ url('projects') }}">Cancel</a>&nbsp;<input type="submit" />

					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
