@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Project</div>

                <div class="panel-body">
                    
					<form method="post" action="{{ url("projects/$project->id") }}">
					<input name="_method" type="hidden" value="PATCH" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
					<label for="project_name">Project Name:</label>
					<input type="text" name="project_name" class="form-control" value="{{ $project->project_name }}" />
					</div>

					<div class="form-group">
					<label for="project_description">Project Name:</label>
					<input type="text" name="project_description" class="form-control" value="{{ $project->project_description }}"  />
					</div>
					<div class="form-group">
						<p class="text-center">
							<a href="{{ url('projects') }}">Cancel</a>&nbsp;<input type="submit" />
						</p>
					</div>
					<div class="form-group">
						<p class="text-right"><a href="#" id="delete" class="btn-sm btn-danger">Delete</a></p>
					</div>

					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
(function($) {
	$('#delete').click(function() {
		$(this).closest('form').find("input[name='_method']").val('DELETE');
		$(this).closest('form').submit();
	});
})(jQuery);
</script>
@endpush