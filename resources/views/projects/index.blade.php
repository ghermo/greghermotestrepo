@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Projects ({{ $project_list->count() }})</div>

                <div class="panel-body">	
					<a href="{{ url('projects/create') }}" class="btn btn-primary">New Project</a>
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Description</th>
								<th>Created By</th>
								<th>Timestamps</th>
								<th>Action</th>
							</tr>
						</thead>
						@foreach($project_list as $project)
						<tr>
							<td>{{$project->project_name}}</td>
							<td>{{$project->project_description}}</td>
							<td>{{$project->user->name}}</td>
							<td>
								Created at: {{$project->created_at}}<br/>
								Updated at: {{$project->updated_at}}<br/>
								Deleted at: {{$project->deleted_at}}<br/>
							</td>
							<td><a href="{{ url("projects/$project->id") }}">View</a>@can('update', $project) | <a href="{{ url("projects/$project->id/edit") }}">Edit</a>@endcan</td>
							
						</tr>
						@endforeach
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
