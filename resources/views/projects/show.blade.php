@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $project->project_name }}&nbsp;<small>{{$project->project_description}}</div>

                <div class="panel-body">
                	<h2>Tasks ({{ $project->tasks->count() }})</h2>
                    <a href="{{ url('projects') }}" class="btn btn-default">Back</a>
					<a href="{{ url("projects/$project->id/create_task") }}" class="btn btn-primary">New Task</a>
					<table class="table">
						<thead>
							<tr>
								<th>Task</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						@foreach($project->tasks as $task)
							<tr>
								<td>{{$task->task}}</td>
								<td>{{$task->status}}</td>
								<td>Edit | Delete</td>
							</tr>
						@endforeach
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
