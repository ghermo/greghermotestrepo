@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Task</div>

                <div class="panel-body">
                    
					<form method="post" action="{{ url("projects/$id") }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
					<label for="task">Task</label>
					<input type="text" name="task" class="form-control" />
					</div>

					<a href="{{ url("projects/$id") }}">Cancel</a>&nbsp;<input type="submit" />

					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
